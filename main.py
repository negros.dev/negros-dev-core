import os
from contextlib import suppress
from pathlib import Path

import yaml
from jinja2 import Environment, FileSystemLoader

basedir = Path(__file__).parent.resolve()


def open_course_dir(dir=basedir / "cursos"):
    try:
        files = os.listdir(dir)
        return [f for f in files if f.endswith(".yaml")]
    except FileNotFoundError:
        raise FileNotFoundError("o diretório cursos não existe! 😑😞😑😞")


def read(file):
    with open(basedir / "cursos" / file, "r") as f:
        return f.read()


def read_yaml(stream):
    return yaml.safe_load(stream)


def render_template(template_name):
    env = Environment(loader=FileSystemLoader(basedir / "jinja"))
    return env.get_template(template_name)


def write_file(file: Path, content: str) -> None:
    try:
        with open(file, "w") as f:
            f.write(content)
        print(f"✅ o curso {file.name} foi publicado com sucesso!")
    except FileExistsError:
        raise FileExistsError(f"{file.name} já existe! 💥")
    except:
        print(f"erro ao tentar publicar o curso {file.name}! 😞")


def create_page(pages: str):
    for page in pages:
        fpath = basedir / "public" / f"{page}.html"
        template = render_template(f"{page}.jinja")
        courses = [read_yaml(read(f)) for f in open_course_dir()]
        output = template.render({"courses": courses})
        write_file(fpath, output)


def create_course_page():
    template = render_template("course.jinja")
    for course in open_course_dir():
        content = read_yaml(read(course))
        output = template.render({"course": content})
        fname = content["slug"] + ".html"

        with suppress(FileExistsError):
            os.makedirs(basedir / "public" / "assistir" / "curso")
        write_file(basedir / "public" / "assistir" / "curso" / fname, output)


if __name__ == "__main__":
    create_page(["index", "sobre"])
    create_course_page()
