import pytest
import yaml


@pytest.fixture(scope="module")
def mock_yaml():
    with open("tests/mock/1.yaml", "r") as f:
        return yaml.safe_load(f.read())
