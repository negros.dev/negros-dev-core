import os
from pathlib import Path
from unittest.mock import patch

from main import open_course_dir
from pytest import mark as m

basedir = Path(__file__).parent.resolve()


@m.it("returns 01 file inside of directory")
@patch.object(os, "listdir")
def test_open_course_dir(mock_dir):
    mock_dir.return_value = ["1.yaml"]
    files = open_course_dir()
    assert len(files) == 1


@m.it("returns file name")
@patch.object(os, "listdir")
def test_open_course_dir_file_name(mock_dir):
    mock_dir.return_value = ["1.yaml"]
    files = open_course_dir()
    assert files[0] == "1.yaml"


@m.it("ensue that be FileNotFoundError")
def test_open_course_dir_FileNotFoundError():
    try:
        open_course_dir(basedir / "tmp")
    except FileNotFoundError as e:
        assert str(e) == "o diretório cursos não existe! 😑😞😑😞"
