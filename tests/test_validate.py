from pytest import mark as m
from validate import Validate


@m.it("returns true if the titulo tag exists!")
def test_validate_has_title(mock_yaml):
    valid = Validate("criando-websites-com-php")
    assert valid.has_title(mock_yaml)


@m.it("returns true if the slug tag exists!")
def test_validate_has_slug(mock_yaml):
    valid = Validate("criando-websites-com-php")
    assert valid.has_slug(mock_yaml)


@m.it("returns true if the criador tag exists!")
def test_validate_has_creator(mock_yaml):
    valid = Validate("criando-websites-com-php")
    assert valid.has_creator(mock_yaml)


@m.it("returns true if the criador_avatar tag exists!")
def test_validate_has_creator_avatar(mock_yaml):
    valid = Validate("criando-websites-com-php")
    assert valid.has_creator_avatar(mock_yaml)


@m.it("returns true if the aula tag exists!")
def test_validate_has_lesson(mock_yaml):
    valid = Validate("criando-websites-com-php")
    assert valid.has_lesson(mock_yaml)


@m.it("returns true if the aula_thumbnail tag exists!")
def test_validate_has_lesson_thumbnail(mock_yaml):
    valid = Validate("criando-websites-com-php")
    assert valid.has_lesson_thumbnail(mock_yaml)


@m.it("returns true if the codigo_fonte tag exists!")
def test_validate_has_source_code(mock_yaml):
    valid = Validate("criando-websites-com-php")
    assert valid.has_source_code(mock_yaml)
