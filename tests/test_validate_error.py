from pytest import mark as m
from validate import Validate


@m.it("should an KeyError, because titulo not exists")
def test_validate_has_title(mock_yaml):
    mock_yaml.pop("titulo", None)
    valid = Validate("criando-websites-com-php")

    try:
        valid.has_title(mock_yaml)
    except KeyError as e:
        assert str(e) == "'faltando titulo em criando-websites-com-php'"


@m.it("should an KeyError, because slug not exists")
def test_validate_has_slug(mock_yaml):
    mock_yaml.pop("slug", None)
    valid = Validate("criando-websites-com-php")

    try:
        valid.has_slug(mock_yaml)
    except KeyError as e:
        assert str(e) == "'faltando slug em criando-websites-com-php'"


@m.it("should an KeyError, because criador not exists")
def test_validate_has_creator(mock_yaml):
    mock_yaml.pop("criador", None)
    valid = Validate("criando-websites-com-php")

    try:
        valid.has_creator(mock_yaml)
    except KeyError as e:
        assert str(e) == "'faltando criador em criando-websites-com-php'"


@m.it("should an KeyError, because criador_avatar not exists")
def test_validate_has_creator_avatar(mock_yaml):
    mock_yaml.pop("criador_avatar", None)
    valid = Validate("criando-websites-com-php")

    try:
        valid.has_creator_avatar(mock_yaml)
    except KeyError as e:
        assert str(e) == "'faltando criador_avatar em criando-websites-com-php'"


@m.it("should an KeyError, because aula not exists")
def test_validate_has_lesson(mock_yaml):
    mock_yaml.pop("aula", None)
    valid = Validate("criando-websites-com-php")

    try:
        valid.has_lesson(mock_yaml)
    except KeyError as e:
        assert str(e) == "'faltando aula em criando-websites-com-php'"


@m.it("should an KeyError, because aula_thumbnail not exists")
def test_validate_has_lesson_thumbnail(mock_yaml):
    mock_yaml.pop("aula_thumbnail", None)
    valid = Validate("criando-websites-com-php")

    try:
        valid.has_lesson_thumbnail(mock_yaml)
    except KeyError as e:
        assert str(e) == "'faltando aula_thumbnail em criando-websites-com-php'"


@m.it("should an KeyError, because codigo_fonte not exists")
def test_validate_has_source_code(mock_yaml):
    mock_yaml.pop("codigo_fonte", None)
    valid = Validate("criando-websites-com-php")

    try:
        valid.has_source_code(mock_yaml)
    except KeyError as e:
        assert str(e) == "'faltando codigo_fonte em criando-websites-com-php'"
