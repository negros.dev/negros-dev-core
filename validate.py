class Validate:
    def __init__(self, filename: str):
        self.filename = filename

    def has_title(self, data):
        if "titulo" in data:
            return True
        raise KeyError(f"faltando titulo em {self.filename}")

    def has_slug(self, data):
        if "slug" in data:
            return True
        raise KeyError(f"faltando slug em {self.filename}")

    def has_creator(self, data):
        if "criador" in data:
            return True
        raise KeyError(f"faltando criador em {self.filename}")

    def has_creator_avatar(self, data):
        if "criador_avatar" in data:
            return True
        raise KeyError(f"faltando criador_avatar em {self.filename}")

    def has_lesson(self, data):
        if "aula" in data:
            return True
        raise KeyError(f"faltando aula em {self.filename}")

    def has_lesson_thumbnail(self, data):
        if "aula_thumbnail" in data:
            return True
        raise KeyError(f"faltando aula_thumbnail em {self.filename}")

    def has_source_code(self, data):
        if "codigo_fonte" in data:
            return True
        raise KeyError(f"faltando codigo_fonte em {self.filename}")
