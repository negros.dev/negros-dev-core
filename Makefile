format:
	@black main.py

check:
	@black main.py --check

test:
	@pytest --it


.PHONY: test check format